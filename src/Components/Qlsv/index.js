import React, { useState } from 'react';
import './qlsv.scss';
const typeMSSV = "MSSV";
const QLSV = () => {
    // [0] : dùng để hiển thị ra ( read only )
    // [1] : dùng để hiển chỉnh sửa ( read only )
    // MSSV1
    // MSSV2

    const [arrSV, setArrSV] = useState([]);
    const [MSSV, setMSSV] = useState(typeMSSV + (arrSV.length + 1));
    const [nameSV, setNameSV] = useState('');
    const [classSV, setClassSV] = useState('');
    const [isEditSV, setIsEditSV] = useState(false);

    const _handleOnChangeNameSV = (e) => {
        const value = e.currentTarget.value;
        if(!value) {
            console.log("is not null");
            return;
        }
        setNameSV(value);
    }
    const _handleOnChangeClassSV = (e) => {
        const value = e.currentTarget.value;
        if(!value) {
            console.log("is not null");
            return;
        }
        setClassSV(value);
    }

    const _handleAddSV = () => {
        if(!nameSV || !classSV){
            console.log("not empty");
            return;
        }
        // rã arrSV ra
        const _arrSV = [...arrSV] || [];
        _arrSV.push({
            mssv: MSSV,
            name: nameSV,
            class: classSV
        })
        setMSSV(typeMSSV + (_arrSV.length + 1))
        setNameSV('');
        setClassSV('');
        return setArrSV(_arrSV);
    };

    const _handleDeleteSV = (_mssv) => {
        const _arrSV = [...arrSV] || [];
        const posSV = _arrSV.findIndex(item => item.mssv === _mssv);
        _arrSV.splice(posSV, 1);

        return setArrSV(_arrSV);
    }
    const _handleGetInfoSVToEdit = (_mssv) => {
        const _arrSV = [...arrSV] || [];
        const posSV = _arrSV.findIndex(item => item.mssv === _mssv);
        if(posSV < 0 && posSV > _arrSV.length -1){
            console.log("MSSV k ton tai");
            return;
        }

        setMSSV(_arrSV[posSV].mssv)
        setNameSV(_arrSV[posSV].name);
        setClassSV(_arrSV[posSV].class);
        setIsEditSV(true);
    };
    const _handleEditSV = () => {
        const _arrSV = [...arrSV] || [];
        const posSV = _arrSV.findIndex(item => item.mssv === MSSV);
        if(posSV < 0 && posSV > _arrSV.length -1){
            console.log("MSSV k ton tai");
            return;
        }
        _arrSV[posSV] = {
            mssv: MSSV,
            name: nameSV,
            class: classSV
        }
        setMSSV(typeMSSV + (_arrSV.length + 1))
        setNameSV('');
        setClassSV('');
        return setArrSV(_arrSV);
    }
    return (
        <div className="wrap_layout_qlsv">
            <div className="wrap_layout_qlsv__container">
                <div className="wrap_layout_input_info">
                    <div className="warp_all_input">
                        <div className="wrap_input">
                            <div className="title">MSSV</div>
                            <input value={MSSV} disabled/>
                        </div>
                        <div className="wrap_input">
                            <div className="title">Name</div>
                            <input onChange={_handleOnChangeNameSV} value={nameSV} />
                        </div>
                        <div className="wrap_input">
                            <div className="title">Class</div>
                            <input onChange={_handleOnChangeClassSV} value={classSV}/>
                        </div>
                        {
                            isEditSV ?
                            <button onClick={_handleEditSV} type="button" className="editSV">Edit SV</button>
                            :
                             <button onClick={_handleAddSV} type="button" className="addSV">Them SV</button>
                        }
                    </div>
                </div>
                <div className="wrap_list_sv">
                    <div className="header_list_sv grid_list_sv">
                        <div>Mssv</div>
                        <div>Name</div>
                        <div>Class</div>
                        <div>Edit</div>
                        <div>Delete</div>
                    </div>
                    <div className="list_sv">
                        {
                            arrSV.map(item => {
                                return (
                                    <div className="item_sv grid_list_sv" key={item.mssv}>
                                        <div>{item.mssv}</div>
                                        <div>{item.name}</div>
                                        <div>{item.class}</div>
                                        <div><button onClick={() => _handleGetInfoSVToEdit(item.mssv)}>Edit</button></div>
                                        <div><button onClick={() => _handleDeleteSV(item.mssv)}>Delete</button></div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}
export default QLSV;