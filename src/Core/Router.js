import React from 'react';
import { Switch, Route} from 'react-router-dom';

import QLSVPage from '../Pages/qlsvpage';

const RouterApp = () => {

    return (
        <Switch>
            <Route path="/qlsv" component={QLSVPage} />
            <Route path="/" component={QLSVPage} />

        </Switch>
    )
}
export default RouterApp;